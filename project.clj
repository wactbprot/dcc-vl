(defproject dccvl "0.1.0"
  :description "Reads CleverLab dcc, generates VacLab json."
  :url "https://gitlab1.ptb.de/bock04/dcc-vl"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.zip "0.1.3"]
                 [com.ashafa/clutch "0.4.0"]]
  :main ^:skip-aot dccvl.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
