```
     _                    _ 
  __| | ___ ___    __   _| |
 / _` |/ __/ __|___\ \ / / |
| (_| | (_| (_|_____\ V /| |
 \__,_|\___\___|     \_/ |_|
                            
```

`dcc-vl` is a study written in [clojure](https://clojure.org) on how to use the CleverLab dcc.
It runs in [REPL](https://clojure.org/guides/repl/introduction) only so far:

```shell
lein repl
#dccvl.core=> 
```
Change from the `user` namespace (`ns`) to `dccvl.core`:

```clojure
user> (in-ns 'dccvl.core)
nil
dccvl.core>
```
Give some configuration information:

```clojure
(def conf {:file "path-to/cleverlab-dcc.xml"
           :id "cob-dmm-se3_voltage_keithley"
           :db {:srv "vaclab_server:5984"
                :db "vl_db"}
           :pos [:CalibrationObject :Result :Voltage]})
```

Run main function:

```clojure
(main conf)
```

Afterwards, the dcc data will be stored in a [CouchDB
document](https://couchdb.apache.org) with the id
`cob-dmm-se3_voltage_keithley` in the database named `vl_db` at the
server `vaclab_server`.

# details

## file->map

```clojure
(def m (file->map f))
(first m)
;; =>
;; [:tag :dcc:digitalCalibrationCertificate]
(last (map->tag-list m :si:label))
;; => 
;; {:tag :si:label, :attrs nil, :content ["Measurement"]}
```
## map->si-lists

```clojure
(def l (map->si-lists m))
(first l)
;; =>
;; [{:tag :si:real,
;;   :attrs nil,
;;   :content
;;   [{:tag :si:label, :attrs nil, :content ["Reference"]}
;;    {:tag :si:value, :attrs nil, :content ["0.00000"]}
;;    {:tag :si:unit,
;;     :attrs nil,
;;     :content
;;     ["\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}"]}]}
;;  {:tag :si:real,
;;   :attrs nil,
;;   :content
;;   [{:tag :si:label, :attrs nil, :content ["Measurement"]}
;;    {:tag :si:value, :attrs nil, :content ["-0.0000042"]}
;;    {:tag :si:unit,
;;     :attrs nil,
;;     :content
;;     ["\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}"]}
;;    {:tag :si:expandedUnc,
;;     :attrs nil,
;;     :content
;;     [{:tag :si:uncertainty, :attrs nil, :content ["0.00000066"]}
;;      {:tag :si:coverageFactor, :attrs nil, :content ["2"]}
;;      {:tag :si:coverageProbability, :attrs nil, :content ["0.95"]}
;;      {:tag :si:distribution, :attrs nil, :content ["normal"]}]}]}]
```

## si-real->vl-map

```clojure
(u/si-real->value-map [{:tag :si:label, :attrs nil, :content ["Reference"]}
	{:tag :si:value, :attrs nil, :content ["0.00000"]}
	{:tag :si:unit,
	:attrs nil,
	:content ["\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}"]}])
;;=>
;; {:Type "cal", :Value 0.0, :Unit "V"}

```

## si-list->vl-map


```clojure
(si-list->vl-map[{:tag :si:real,
	:attrs nil,
	:content
	[{:tag :si:label, :attrs nil, :content ["Reference"]}
	{:tag :si:value, :attrs nil, :content ["0.00000"]}
	{:tag :si:unit,
		:attrs nil,
		:content
		["\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}"]}]}
	{:tag :si:real,
	:attrs nil,
	:content
		[{:tag :si:label, :attrs nil, :content ["Measurement"]}
			{:tag :si:value, :attrs nil, :content ["-0.0000042"]}
		{:tag :si:unit,
			:attrs nil,
		:content
		["\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}"]}
		{:tag :si:expandedUnc,
		:attrs nil,
		:content
		[{:tag :si:uncertainty, :attrs nil, :content ["0.00000066"]}
		{:tag :si:coverageFactor, :attrs nil, :content ["2"]}
		{:tag :si:coverageProbability, :attrs nil, :content ["0.95"]}
		{:tag :si:distribution, :attrs nil, :content ["normal"]}]}]}])
;; =>
;; [{:Type "cal", :Value 0.0, :Unit "V"}
;;  {:Type "ind", :Value -4.2E-6, :Unit "V"}]
```
