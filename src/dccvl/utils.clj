(ns dccvl.utils
  ^{:author "wactbprot"
    :doc "dccvl utils."}
  (:require [clojure.xml :as xml]
            [clojure.java.io :as io]
            [clojure.zip :as zip]))

(defn file->map
  "Returns a map based on the xml stucture stored in file `f`."
  [f]
  (-> f
      io/input-stream
      xml/parse))

(defn map->tag-list
  "Returns a lazy seq of all structs with `(= (:tag node) tag)`." 
  [m tag] 
  (->> (zip/xml-zip m)
       (iterate zip/next)
       (take-while (complement zip/end?))
       (map zip/node)
       (filter (fn [node] (and (associative? node)
                               (= (:tag node) tag))))
       (map :content)))

;;-------------------------------------------------------
;; si-list filter
;;-------------------------------------------------------
(defn map->si-lists
  [m]
  (map->tag-list m :si:list))

;;-------------------------------------------------------
;; map to vaclab map
;;-------------------------------------------------------
(defmulti map->vl-map
  "Given map is transformed depending on `:tag`."
  :tag)

(defmethod map->vl-map :si:label
  [{tag :tag content :content}]
  (let [tag-trans {"Measurement"  "ind"
                   "Reference" "cal"}]
  {:Type (get tag-trans (first content))}))

(defmethod map->vl-map :si:value
  [{tag :tag content :content}]
  {:Value (Double/parseDouble (first content))})

(defmethod map->vl-map :si:unit
  [{tag :tag content :content}]
  (let [unit-trans {"\\kilogram\\metre\\tothe{2}\\second\\tothe{-3}\\ampere\\tothe{-1}" "V"}]
  {:Unit (get unit-trans (first content))}))

(defmethod map->vl-map :default
  [x])

;;-------------------------------------------------------
;; si-real to vaclab map
;;-------------------------------------------------------
(defn si-real->vl-map
  "Extracts values from a `si:real` structure."
  [v]  
  (into {}
        (map map->vl-map v)))

;;-------------------------------------------------------
;; si-list tu vaclab vector
;;-------------------------------------------------------
(defn si-lists->vl-vec 
  "Extracts a certain column by means
  of column function `col-fn`. Turns the `si:real`
  in `:content` into vaclab style map by `si-real->vl-map`."
  [l col-fn]
  (->> l
       (mapv col-fn)
       (mapv :content)
       (mapv si-real->vl-map)))

;;-------------------------------------------------------
;; vaclab style map
;;-------------------------------------------------------
(defn vl-map
  "Groups the given vector `v` following `m`.
  Returns a vaclab style map."
  [v m]
  (let [v (mapv v (range (:start m) (:stop m)))]
    {:Type  (str (:prefix m) "_" (:Type (first v)))
     :Value (into [] (map :Value v))
     :Unit  (:Unit (first v))}))


;;-------------------------------------------------------
;; all
;;-------------------------------------------------------
(defn dcc->vl-result
  "Generates the vaclab style result." 
  [m conf]
  (let [l    (map->si-lists m)
        refe (si-lists->vl-vec l first) 
        meas (si-lists->vl-vec l second)]
    (into []
          (concat (mapv (fn [m] (vl-map refe m)) conf)
                  (mapv (fn [m] (vl-map meas m)) conf)))))
