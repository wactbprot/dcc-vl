(ns dccvl.db
  (:require [com.ashafa.clutch :as couch]))

(defn conn
  [c]
  (let [usr (System/getenv "CAL_USR")
        pwd (System/getenv "CAL_PWD")]
    (if (and usr pwd)
      (str "http://" usr ":" pwd "@"(:srv c) "/"(:db c))
      (str "http://" (:srv c) "/"(:db c)))))

(defn get-doc
  "Gets a document with `id`."
  [id conf]
  (couch/get-document (conn conf) id))

(defn put-doc
  "Saves a document."
  [doc conf]
  (couch/put-document (conn conf) doc))
