(ns dccvl.core
  ^{:author "wactbprot"
    :doc "Gets data out of dcc xml file and stores it in vaclab style."}
  (:require  [dccvl.utils :as u]
             [dccvl.db :as db]
             [dccvl.cert :as cert]
             [clojure.string :as str]))


(def conf {:file "/home/thomas/com/vl-dcc/examples/Keithley_DAQ6510_4427944.xml"
           :id "cob-dmm-se3_voltage_keithley"
           :db {:srv "a73434.berlin.ptb.de:5984"
                :db "vl_db"}
           :pos [:CalibrationObject :Result :Voltage] 
           :no-of-channels 9})

(defn main
  [{f :file db :db cert :cert-plan id :id pos :pos no-of-channels :no-of-channels}]
  (let [d (db/get-doc id db)
        m (u/file->map f)
        r (u/dcc->vl-result m (cert/plan no-of-channels))]
    (db/put-doc (assoc-in d pos r) db)))
