(ns dccvl.cert
  ^{:author "wactbprot"
    :doc "Constructs a plan for disassembling the dcc structure."}
  (:require  [clojure.string :as str]))

(defn channel-plan
  "Builds up a channel plan.

  Example:
  ```clojure
  (cert/channel-plan 0 front)
  
  ;; =>
  ;; [{:prefix front_offset_0.1V, :start 0, :stop 1}
  ;; {:prefix front_calib_0.1V, :start 1, :stop 5}
  ;; {:prefix front_offset_1V, :start 5, :stop 6}
  ;; {:prefix front_calib_1V, :start 6, :stop 10}
  ;; {:prefix front_offset_10V, :start 10, :stop 11}
  ;; {:prefix front_calib_10V, :start 11, :stop 31}
  ;; {:prefix front_offset_100V, :start 31, :stop 32}
  ;; {:prefix front_calib_100V, :start 32, :stop 36}]
  "
  [j ch]
  (let [delta   3
        n=0     8
        n>0     6
        n       (if (= j 0) n=0 n>0)
        off=0   36
        off>0   32
        border  (take (+ n 1) [0 1 5 6 10 11 31 32 36])
        j-1     (- j 1)
        end>0   (- (+ off=0 (* j-1 off>0)) j-1)
        end     (if (= j 0) 0 end>0)
        prefix  (interleave
                 (take n (repeat ch))
                 (take n (cycle ["offset" "calib"]))
                 (take n '("0.1V" "0.1V" "1V" "1V" "10V" "10V" "100V" "100V")))]
    (mapv
     (fn [i]
       {:prefix (str/join "_" (take delta (drop (* i delta) prefix)))
        :start (+ end (nth border i))
        :stop  (+ end (nth border (+ 1 i)))})
     (range n))))

(defn plan
  "Builds the plan for the certificate.

  Example:
  ```clojure
  (cert/plan 2)
  
  ;; =>
  ;;  [{:prefix ch0_offset_0.1V, :start 0, :stop 1}
  ;; {:prefix ch0_calib_0.1V, :start 1, :stop 5}
  ;; {:prefix ch0_offset_1V, :start 5, :stop 6}
  ;; {:prefix ch0_calib_1V, :start 6, :stop 10}
  ;; {:prefix ch0_offset_10V, :start 10, :stop 11}
  ;; {:prefix ch0_calib_10V, :start 11, :stop 31}
  ;; {:prefix ch0_offset_100V, :start 31, :stop 32}
  ;; {:prefix ch0_calib_100V, :start 32, :stop 36}
  ;; {:prefix ch1_offset_0.1V, :start 36, :stop 37}
  ;; {:prefix ch1_calib_0.1V, :start 37, :stop 41}
  ;; {:prefix ch1_offset_1V, :start 41, :stop 42}
  ;; {:prefix ch1_calib_1V, :start 42, :stop 46}
  ;; {:prefix ch1_offset_10V, :start 46, :stop 47}
  ;; {:prefix ch1_calib_10V, :start 47, :stop 67}]
  ```
  "
  [no-of-channels]
  (let [delta   2
        start   0
        all-ch  (interleave
                 (take no-of-channels (repeat "ch"))
                 (map str (range start no-of-channels)))
        comb-ch (mapv
                 (fn [i]
                   (str/join "" (take delta (drop (* i delta) all-ch))))
                 (range no-of-channels))]
    (into [] (flatten (map-indexed channel-plan comb-ch)))))
